package lesson_11;

import de.vandermeer.asciitable.AsciiTable;

public class AsciiTableExample {

  public static void main(String[] args) {

    AsciiTable at = new AsciiTable();
    at.addRule();
    at.addRule();
    at.addRow("Ad", "Soyad", "Yaş");
    at.addRule();
    at.addRow("Əlizamin", "Quliyev", "30");
    at.addRule();
    at.addRow("Orxan", "Həsənli", "30");
    at.addRule();
    String table = at.render();
    System.out.println(table);

  }

}
