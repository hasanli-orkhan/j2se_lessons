package lesson_11;

import info.md7.cbar_currency.exceptions.CurrencyNotFoundException;
import info.md7.cbar_currency.exceptions.IncorrectContentTypeException;
import info.md7.cbar_currency.exceptions.SpecifiedDateIsAfterException;
import info.md7.cbar_currency.model.Currency;
import info.md7.cbar_currency.model.CurrencyCode;
import info.md7.cbar_currency.util.CurrencyConverter;
import info.md7.cbar_currency.util.CurrencyRate;
import java.math.BigDecimal;
import java.time.LocalDate;

public class CbarExample {
    public static void main(String ... args)
        throws SpecifiedDateIsAfterException, CurrencyNotFoundException, IncorrectContentTypeException {

      /*
       * 1 t.u - 31.1034768
       * x t.u - 50
       */

      double t_u = 50 / 31.1034768;
      System.out.println(t_u);

    /*
        1 t.u - 3257.489 azn
        1.6 t.u - x azn
     */

      Currency xau_old = CurrencyRate.getCurrencyRateForDate(CurrencyCode.XAU, LocalDate.of(2017, 2, 4));
      Currency xau = CurrencyRate.getActualCurrencyRate(CurrencyCode.XAU);
      double AZN_old = t_u * xau_old.getValue().doubleValue();
      double AZN = t_u * xau.getValue().doubleValue();
      System.out.println(AZN_old);
      System.out.println(AZN);

    }

}
