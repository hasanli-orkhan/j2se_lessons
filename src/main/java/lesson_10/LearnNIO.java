package lesson_10;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashSet;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class LearnNIO {

  public static void main(String[] args) throws IOException {

    Path path = Paths.get("/Users/orkhan-hasanli/Desktop/test.txt");
    List<String> lines = Files.readAllLines(path);
    for (String line : lines) {
      System.out.println(line);
    }

//    Path directory = Files.createDirectory(Path.of("/Users/orkhan-hasanli/Desktop/demo"));
    if (Files.exists(Path.of("/Users/orkhan-hasanli/Desktop/demo/test.txt"))) {
      System.out.println("Fayl qovluqda var");
    }

//    Files.copy(path, Path.of(directory + "/test.txt"));

    List<Path> result;
    try (Stream<Path> walk = Files.walk(Path.of("/Users/orkhan-hasanli/Desktop"))) {
      result = walk.filter(Files::isRegularFile)
          .collect(Collectors.toList());
    }
    System.out.println("--------------");
    for (Path currentPath : result) {
      System.out.println(currentPath.getFileName());
    }


  }

}
