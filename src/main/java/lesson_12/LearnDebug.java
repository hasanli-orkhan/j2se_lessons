package lesson_12;

import java.util.ArrayList;
import java.util.List;

public class LearnDebug {

  public static void main(String[] args) {

    int[] numbers = { 1, 2, 3, 4, 5 };
    printArray(numbers);

  }

  public static void printArray(int[] numbers) {
    for (int i = 0; i < 6; i++) {
      System.out.println(numbers[i]);
    }
  }

}
