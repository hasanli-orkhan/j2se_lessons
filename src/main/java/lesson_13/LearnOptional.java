package lesson_13;

import java.util.Optional;

public class LearnOptional {

  public static void main(String[] args) {

    // Imperative paradigm
    User user = new User("test1", "test2", "test3");
    if (user != null) {
      System.out.println(user.getName());
    } else {
      System.out.println("User is null");
    }


    // Declarative paradigm
    Optional<User> userOptional = Optional.ofNullable(null);

    userOptional.ifPresent(System.out::println);

    if (userOptional.isPresent()) System.out.println(userOptional.get());

    User user2 = userOptional.orElseThrow(() -> new RuntimeException("Account doesn't exist"));
    System.out.println(user2);

    User user3 = userOptional.orElse(new User("orkhan", "hasanli", "ruhin"));
    System.out.println(user3);






  }

}
