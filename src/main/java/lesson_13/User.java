package lesson_13;

import java.util.Objects;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
@EqualsAndHashCode
public class User {

  private String name;
  private String surname;
  private String patronymic;

}

class Main  {
  public static void main(String[] args) {
    User user = new User("hello", "world", "java");
    System.out.println(user);
  }
}