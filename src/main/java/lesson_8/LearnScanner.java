package lesson_8;

import java.util.Scanner;

public class LearnScanner {

  static String name;
  static String surname;

  static {
    name = "Məmməd";
    surname = "Məmmədov";
  }

  public static void main(String[] args) {

    System.out.println("Hello world");

    Scanner scanner = new Scanner(System.in);
    System.out.println("Adınızı daxil edin:");
    String name = scanner.nextLine();
    System.out.println("Doğum ilinizi daxil edin:");
    int dobYear = scanner.nextInt();
    int age = LearnDate.getAge(dobYear);
//    System.out.println("Sizin adınız: " + name + " Sizin yaşınız: " + age);
    System.out.printf("Sizin adınız: %s Sizin yaşınız: %s", name, age);

  }

}
