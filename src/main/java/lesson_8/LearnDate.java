package lesson_8;

import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Month;
import java.time.ZoneId;
import java.time.chrono.HijrahDate;
import java.time.format.DateTimeFormatter;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

public class LearnDate {

  public static void main(String[] args) {

    Date date = new Date();
    System.out.println(date);

    // UNIX time
    // 1 san = 1000 ms
    System.out.println(date.getTime());

    // dd-MM-yyyy
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd.MM.yyyy HH:mm z");
    String formattedDate = simpleDateFormat.format(date);
    System.out.println(formattedDate);

    int age = getAge(1980);
    System.out.println(age);

    LocalDate localDate = LocalDate.now();
    System.out.println(localDate);

    String formattedDateTime = localDate.format(DateTimeFormatter.ofPattern("dd/MM/yyyy"));
    System.out.println(formattedDateTime);

    LocalDate randomDate = LocalDate.of(2023, Month.JANUARY, 17);
    boolean after = randomDate.isBefore(localDate);
    System.out.println(after);

    LocalDateTime localDateTime = LocalDateTime.now();
    System.out.println(localDateTime);

    String formattedDateTime1 = localDateTime.format(DateTimeFormatter.ofPattern("dd.MM.yyyy HH:mm"));
    System.out.println(formattedDateTime1);
    LocalDateTime parsedDateTime = LocalDateTime.parse("14-01-2023 15:45",
        DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm"));
    System.out.println(parsedDateTime);

    Instant current = Instant.now();
    System.out.println(current);
    long epochSecond = current.getEpochSecond();
    long aLong = current.atZone(ZoneId.systemDefault()).toInstant().toEpochMilli();
    System.out.println(aLong);

    HijrahDate hijrahDate = HijrahDate.now();
    System.out.println(hijrahDate);

    GregorianCalendar gregorianCalendar = new GregorianCalendar();
    Date time = gregorianCalendar.getTime();
    System.out.println(time);

  }

  public static int getAge(int dobYear) {
    Calendar calendar = Calendar.getInstance();
    return calendar.get(Calendar.YEAR) - dobYear;
  }


}
