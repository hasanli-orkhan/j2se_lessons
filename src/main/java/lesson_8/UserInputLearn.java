package lesson_8;

import java.io.IOException;
import java.util.Scanner;

public class UserInputLearn {

  public static void main(String[] args) throws IOException {
    Scanner scanner = new Scanner(System.in);
    System.out.println("Mətn daxil edin");
    String next = scanner.next();
    System.out.println(next);

    System.out.println("Rəqəm daxil edin");
    int i = scanner.nextInt();
    System.out.println(i);
  }

}
