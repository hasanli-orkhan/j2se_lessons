package lesson_8;

import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.chrono.HijrahDate;
import java.time.temporal.TemporalField;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

// https://www.tutorialspoint.com/java/java_date_time.htm
public class LearnDateTime {

  public static void main(String[] args) {

    Date date = new Date();
    System.out.println(date);

    long time = date.getTime();
    System.out.println(time); // in millis

    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
    String formattedDate = sdf.format(date);
    System.out.println(formattedDate);

    Calendar calendar = Calendar.getInstance();
    System.out.println(calendar.get(Calendar.YEAR));

    LocalDate localDate = LocalDate.now();
    System.out.println(localDate.getYear());

    LocalTime localTime = LocalTime.now();
    System.out.println(localTime);

    LocalDateTime localDateTime = LocalDateTime.now();
    System.out.println(localDateTime);

    Instant instant = Instant.now();
    System.out.println(instant.getEpochSecond()); // in seconds

    HijrahDate hijrahDate = HijrahDate.now();
    System.out.println(hijrahDate);



  }

}
