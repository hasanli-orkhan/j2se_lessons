package lesson_9;



import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class WritingToFile {

  public static void writeData(String filePath, String text) {
    try (FileOutputStream fileOutputStream = new FileOutputStream(filePath, true)) {
      byte[] bytes = text.getBytes();
      fileOutputStream.write(bytes);
    } catch (FileNotFoundException e) {
      throw new RuntimeException(e);
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
  }

}
