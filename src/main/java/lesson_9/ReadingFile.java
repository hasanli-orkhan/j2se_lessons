package lesson_9;

import static lesson_9.WritingToFile.writeData;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ReadingFile {

  protected static String data;

  public static void main(String[] args) {

    String filePath = "/Users/orkhan-hasanli/Desktop/hello.txt";

    // try ... catch ... finally
    FileInputStream fileInputStream = null;
    try {
      fileInputStream = new FileInputStream(filePath);
      byte[] bytes = fileInputStream.readAllBytes();
      String text = new String(bytes);
      System.out.println(text);

    } catch (FileNotFoundException e) {
      System.out.println("Fayl tapılmadı!");
    } catch (IOException e) {
      System.out.println("IO istisnası baş verdi");
    } finally {
      try {
        fileInputStream.close();
      } catch (IOException e) {
        System.out.println("Stream bağlanıla bilmədi!");
      }
    }

    File file = new File(filePath);
    if (file.exists()) {
      System.out.println("Fayl yaradıldı");
    }
    writeData(filePath, "Axır ki fayl yaratdıq");
    readDataWithFileInputStream(filePath);



//    String directory = "/Users/orkhan-hasanli/Desktop/tesst/demo/elizamin";
//    File file = new File(directory);
//    file.mkdirs();



  }



  public static void readDataWithFileInputStream(String filePath) {
    try (FileInputStream fileInputStream = new FileInputStream(filePath)) {
      byte[] bytes = fileInputStream.readAllBytes();
      String text = new String(bytes);
      System.out.println(text);
    } catch (FileNotFoundException e) {
      System.out.println("Fayl tapılmadı!");
    } catch (IOException e) {
      System.out.println("IO istisnası baş verdi");
    }
  }

  public static void readDataWithFileReader(String filePath) {
    try (FileReader reader = new FileReader(filePath)) {
      List<Character> characters = new ArrayList<>();
      int content;
      while ((content = reader.read()) != -1) {
        characters.add((char) content);
      }
      String text = concatWords(characters);
      System.out.println(text);

    } catch (FileNotFoundException e) {
      throw new RuntimeException(e);
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
  }

  private static String concatWords(List<Character> characters) {
    StringBuilder stringBuilder = new StringBuilder();
    for (int i = 0; i < characters.size(); i++) {
      stringBuilder.append(characters.get(i));
    }
    return stringBuilder.toString();
  }


}
