package lesson_5;

public class StringAndStringBuilder {

  public static void main(String[] args) {
    String text = "hello";
    int index = text.indexOf('e');
    System.out.println(index);

    char character = text.charAt(4);
    System.out.println(character);

    boolean isExists = text.contains("ell");
    System.out.println(isExists);

    boolean isStartsWith = text.startsWith("h");
    System.out.println(isStartsWith);

    System.out.println(text.toUpperCase());

    System.out.println(text.replaceAll("l", "n"));
  }

}
